
//Theme Light/Dark
const BgTheme = document.querySelector('#bg-theme')
const themeLogo = document.querySelector('#moon');
const body = document.querySelector('body');
const navbar = document.querySelector('nav');
const navbarToggler = document.querySelector('#navbar-toggle-icon');
const navBrand = document.querySelector('.nav-brand');
const navLinks = document.querySelectorAll('a.nav-link');

window.onload = () => {
    const cookies = decodeURIComponent(document.cookie);
    const cookieItems = cookies.split(';');

    if(cookieItems[0] === 'theme=dark') {
        body.classList.add('bg-dark');
        navbar.classList.add('bg-dark');
        body.classList.add('text-info');
        navLinks.forEach(function(i){
            i.classList.add('text-info');
        })
        navbarToggler.classList.remove('text-dark');
        themeLogo.classList.remove('text-dark');
    }
}

BgTheme.addEventListener('click', () => {
    if(body.classList.contains('bg-dark')){
        body.classList.remove('bg-dark');
        navbar.classList.remove('bg-dark');
        body.classList.remove('text-info');
        themeLogo.classList.add('text-dark');
        navbarToggler.classList.add('text-dark');
        navLinks.forEach(function(i){
            i.classList.add('text-info');
        })
        document.cookie = "theme=light";
        console.log(document.cookie)
    } else {
        body.classList.add('bg-dark');
        navbar.classList.add('bg-dark');
        body.classList.add('text-info'); 
        themeLogo.classList.remove('text-dark');
        navLinks.forEach(function(i){
            i.classList.remove('text-info');
        })
        navbarToggler.classList.remove('text-dark');
        document.cookie = "theme=dark";
        console.log(document.cookie)
    }
    
})





const showMoreAbout = document.querySelector('#show-more');
const showMoreButton = document.querySelector('#show-more-btn');
const angleArrow = document.querySelector('.fa-angle-down');


showMoreButton.addEventListener('click', () => {
    angleArrow.classList.toggle('fa-angle-up');
})