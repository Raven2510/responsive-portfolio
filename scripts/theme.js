$(function(){
    const cookies = decodeURIComponent(document.cookie);
    const cookieTheme = cookies.split(';');
    let theme = cookieTheme[0].split('=')[1];

    if(theme === "dark"){
        $('body, .navbar').addClass(`bg-${theme}`);
        $(`p[data-text-type="normal"]`).addClass('text-warning');
        $('#moon, .fa-bars').addClass('text-light');
    } else {
        document.cookie = 'theme=light';
    }

    $('#moon').click(function(){
        $('body, .navbar').toggleClass('bg-dark');
        $(`p[data-text-type="normal"]`).toggleClass('text-warning');
        $('#moon, .fa-bars').toggleClass('text-light');
        
        if(theme === "dark"){
            document.cookie = 'theme=light;';
        } else {
            document.cookie = 'theme=dark;';
        }

    });
});